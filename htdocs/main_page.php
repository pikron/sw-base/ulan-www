<h1>What is uLan</h1>
<p>
uLan provides 9-bit message oriented communication protocol, which
is transferred over an RS-485 link. Characters are transferred same way
as for RS-232 asynchronous transfer except parity bit, which is used
to distinguish between data characters and protocol control information.
A physical layer consists of one twisted pair of leads and RS-485
transceivers.

<p>
Use of 9-bit character simplifies a transfer of binary data and for
intelligent controllers can lower the CPU load, because of the CPU
need not care about data characters send to other nodes. Producers
of most microcontrollers for embedded applications know that and have
implemented 9-bit extension in UARTs of most of today's MCUs. There
is the list below to mention some of them:
<p>
<ul>
<li> many ARM based MCUs and SOCs (&nbsp;NXP LPC 17xx, 4xxx, ...&nbsp;)</li>
<li> members of Motorola 683xx family (&nbsp;68332, 68376, ...&nbsp;)</li>
<li> Hitachi H8 microcontrollers</li>
<li> all Intel 8051 and 8096 based MCUs with UART</li>
</ul>
<p>
Intel has developed a multiprotocol UART i82510, which is very well
suited for implementing 9-bit communication interface for PC computers.
The second example of the chip, which is well suited for 9-bit communication,
is OX16C954-PCI produced by Oxford Semiconductors.

<p>
One of the problems of 9-bit communications is missing standardization
of message protocol. Drivers and formats of one possible implementation
of uLan message protocol are provided by this project.
<p>
