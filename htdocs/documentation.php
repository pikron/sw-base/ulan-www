<h1>Documentation</h1>
<p>

<dt><a href="index.php?page=3">ulan Driver and Protocol Base Documentation (ul_drv)</a><a href="pdf/ul_drv.pdf">(PDF)</a></dt>
  <dd>Contents: What is uLan, Message Protocol, uLan Driver for Linux and Windows,
      uLan Interface and Services - network management and common applications commands,
      uLan Object Interface Layer - short introduction and common objects IDs numbers</dd>

<dt><a href="index.php?page=4">uLan Protocol and Its Utilization in Applications Presentation</a></dt>
  <dd>Contents: Little about uLan History, uLan Message Protocol, End User Applications
     (CHROMuLAN System, Automatic Feeding System,  Home Automation)</dd>

<dt><a href="pdf/ulan-pdocochan-rtlws13.pdf" target="_blank">Process Data Connection
       Channels in uLan Network for Home Automation and Other Distributed Applications
       (PDF)</a></dt>
  <dd>The paper published at <a href="http://www.osadl.org/RTLWS-2011.rtlws-2011.0.html">13<sup>th</sup> Real-Time Linux Workshop</a>. The main focus of the paper is placed on publisher-subscriber data communication channels service implemented for uLAN home automation applications.</dd>

<dt><a href="pdf/ulan-pdocochan-slides-rtlws13.pdf" target="_blank">uLAN Presentation Slides (PDF)</a></dt>
  <dd>from above mentioned conference</dd>

<dt><a href="pdf/ulan-poster-pc05.pdf" target="_blank">uLan Protocol and Its Utilization in Applications - Poster (PDF)</a></dt>
  <dd>The poster presented on the conference Process Control 2005</dd>

<dt><a href="man/index.html" target="_blank">uLan Project Utilities Manual Pages</a></dt>
  <dd>ul_asc, ul_asd, ul_dysn, ul_lcabsp, ul_lcscan, ul_oitool, ul_pdotool, ul_sendhex,
      ul_sendmsg, ul_spy, usb_sendhex,
  </dd>

<dt><a href="pdf/ulut.pdf" target="_blank">uLUt Library - uLan/Universal Light Utilities Library</a></dt>
  <dd>Double linked lists, Generalized AVL trees (GAVL), hierarchical timers framework (HTIMER),
      Dynamic buffer(DBUFF), Event connectors infrastructure (EVC) etc.
  </dd>


<dt><a href="pdf/ulevpoll.pdf" target="_blank">uLEvPoll Library - uLan/Universal Light Event Poll Library</a></dt>
  <dd>Minimal but highly environment agnostic system events processing library supporting select,poll, epoll,
      libevent and Gtk+/Glib based main loop processing.
  </dd>

</p>
