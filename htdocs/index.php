<?php include('register_globals.php');register_globals(); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="stylesheet" type="text/css" href="printstyle.css" media="print" />  
  <link rel="stylesheet" type="text/css" href="screenstyle.css" media="screen" />
<title>uLan communication</title>
</head>
<body>
<div class="wholepage">

<div id="header">
		 <div class="superheader">http://ulan.sourceforge.net/</div>
		 <div class="mainheader"><h1>uLan communication</h1><p>&ldquo;Open Source implementation of a multi-master communication protocol&rdquo;</p></div>
		 <div class="subheader">Welcome to you!</div>
</div>

<div id="sidecontainer">
		 <h2>Navigation</h2>
		 <ul class="nav">
		 <li><a href="index.php">Home</a></li>
		 <li><a href="index.php?page=1">Documentation</a></li>
		 <li><a href="index.php?page=5">Screenshots</a></li>
		 <li><a href="index.php?page=2">Download</a></li>
		 <li><a href="http://www.ohloh.net/p/ulan"><img src="http://www.ohloh.net/p/ulan/widgets/project_thin_badge.gif" alt="@ohloh" width="100" height="16" /></a></li>
		 </ul>
		 <h2>Links</h2>
		 <ul class="nav">
		 <li><a href="http://www.sf.net/projects/ulan" title="Project Summary Page at SF.net">Project Summary Page</a></li>
		 <li><a href="http://sourceforge.net/p/ulan/_list/git" title="Project GIT repositoty">Project GIT repository</a></li>
		 <li><a href="https://sourceforge.net/p/ulan/wiki/" title="Project Wiki Pages">Project Wiki Pages</a></li>
		 <li><a href="http://chromulan.org/" title="CHROMuLAN">CHROMuLAN</a></li>
		 <li><a href="http://cmp.felk.cvut.cz/~pisa/" title="Pavel Pisa home page">Pavel Pisa home page</a></li>
		 <li><a href="http://www.sf.net/" title="Sourceforge Site">SourceForge</a></li>
		 <li><a href="http://www.oswd.org/" title="Open Source Web Design">OSWD</a></li>
		 </ul>
		 <br>
		 <!--
		 <center>
		 <IMG SRC="http://pocitadlo.co.cz/cgi-bin/wc/-d/5/-z/ulan">
		 </center>
		 -->
</div>

<div id="content">

      <?
      switch ($page) {
        case 1:
          include('documentation.php');
          break;
        case 2:
          include('download.php');
          break;
        case 3:
          include('ul_drv.html');
          break;
        case 4:
          include('ulan-paper-pc05.html');
          break;
        case 5:
          include('screenshots.php');
          break;
        default:
          include('main_page.php');
          break;
      }
      $page=0;

      ?>


</div>

<div id="footer">
		 <div class="superfooter"></div>
		 <div class="mainfooter">
		 <p>Copyright &copy; Whatsisname Thingymajig 2005</p>
		 <p>Daisy Photo Credit: <strong><a href="http://www.sxc.hu/browse.phtml?f=profile&amp;l=tzankov">Dimiter Tzankov</a></strong>&nbsp;&nbsp;::&nbsp;&nbsp;Design by <strong><a href="http://www.oswd.org/userinfo.phtml?user=ad_267">Ad_267</a></strong></p>
		 </div>
		 <div class="subfooter"></div>
</div>


</div>
</body>
</html>
