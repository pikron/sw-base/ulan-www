<h1>Screenshots</h1>

<h3><a href="https://sourceforge.net/p/ulan/ulan-app/ci/master/tree/app-host/uloi_browser/">uLAN Object Interface Browser</a><h3>
<p>
version 0.1.2 - array, string support
</p>
<center>
<table border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center">
    <IMG NAME="uloi_browser1" SRC="img/uloi_browser_0_1_2_linux.png" WIDTH=600 BORDER=0 ALT="uLAN Object Interface Browser (Linux)">
    <br>
    uloi_browser in Linux OS, GTK2
    </td>
  </tr>
  <tr>
    <td align="center">
    <IMG NAME="uloi_browser2" SRC="img/uloi_browser_0_1_2_windows.png" WIDTH=600 BORDER=0 ALT="uLAN Object Interface Browser (Windows)">
    <br>
    uloi_browser in Windows Vista OS
    </td>
  </tr>
</table>
</center>

<h2>Projects and Solutions which Use uLAN Communication<h2>
<h3><a href="http://www.pikron.com/pages/products/hplc/chromulan.html">CHROMuLAN</a><h3>
<p>
Open-source chromatography system for everyone. Source forge project page <a href="https://sourceforge.net/projects/chromulan/">https://sourceforge.net/projects/chromulan/</a>.
</p>
<center>
<table border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center">
    <IMG NAME="cul1" SRC="img/chromulan-scr3.png" WIDTH=500 HEIGHT=333 BORDER=0 ALT="CHROMuLAN Open-source Chromatography System">
    <br>
    CHROMuLAN version 0.92 running on Debian GNU/Linux system
    </td>
  </tr>
  <tr>
    <td align="center">
    <IMG NAME="cul2" SRC="img/cul-and-lc5000-sm.jpg" WIDTH=500 HEIGHT=252 BORDER=0 ALT="PiKRON Instruments Controlled by CHROMuLAN over uLAN">
    <br>
    CHROMuLAN controlling HPLC instruments setup
    </td>
  </tr>
</table>
</center>

<h3><a href="https://www.openchrom.net/">OpenChrom</a><h3>
<p>
Open source software for chromatography and mass spectrometry based on the Eclipse Rich Client Platform (RCP).
The CHROMuLAN plugin has been developed for this system which allows to run data acquisition from uLAN connected devices from the OpenChrom system environment.
</p>
<center>
<table border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td align="center">
    <IMG NAME="cul1" SRC="img/openchrom-cul-16-sample-view-1.png" WIDTH=600 BORDER=0 ALT="OpenChrom Running Data Acquisition from uLAN Connected Devices">
    <br>
    OpenChrom with data acqusition plugin, <a href="http://pikron.com/pages/products/hplc/lcd_5000.html">LCD5000</a> and <a href="http://pikron.com/pages/products/hplc/ulad_32.html">ULAD32</a> aquisition channels
    </td>
  </tr>
</table>
</center>
